<?php

use SideKit\Config\ConfigKit;

return [
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        '<controller:(about)>' => 'site/index',
        '<controller:(\w|-)+>/' => '<controller>/index',
        '<controller:\w+>/<action:(\w|-)+>/<id:\d+>' => '<controller>/<action>',
        '<module:\w+>/<controller:\w+>/<action:(\w|-)+>' => '<module>/<controller>/<action>',
        '<controller:\w+>/<action:(\w|-)+>' => '<controller>/<action>'
    ],
];
